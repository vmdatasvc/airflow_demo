#!/bin/bash


cp -r /home/ubuntu/airflow_demo/main/dags /home/ubuntu/airflow
cp -r /home/ubuntu/airflow_demo/main/config/airflow.cfg /home/ubuntu/airflow
cd /home/ubuntu/airflow/dags
chmod +x ./Face_Postprocessor
cd /home/ubuntu/airflow
airflow worker -D
rm -r /home/ubuntu/airflow_demo