#!/bin/bash
# Script to setup the dependencies for the sample app. Designed to work with
# Amazon Linux.

# Make sure Python PIP is available on the system.
apt-get update -y
apt-get install gcc python-devel python-pip -y
apt-get install -y docker.io
apt-get install -y libcurl4-openssl-dev
apt-get install -y libmysqlcppconn-dev
apt-get install -y libpthread-stubs0-dev
apt-get install -y libssl-dev
# apt-get install -y python-dev python-pycurl python-simplejson
# apt-get install libopencv-dev python-opencv

# start docker service 
service docker start
# add ec2-user to the docker group so that you can excute Docker commands without using sudo
usermod -a -G docker ec2-user

# Install docker.
apt-get install -y python-pip
pip install tornado
pip install pyyaml
pip install bs4
pip install lxml
pip install boto3
pip install psycopg2
pip install pycurl
pip install flask_bcrypt

# export EMAIL = isabella6151@gmail.com
# export PASSWORD = irrelev@nt
