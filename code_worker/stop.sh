#!/bin/bash
# Stop the web server.

cd /home/ubuntu/airflow
kill `sudo cat airflow-worker.pid | xargs`

# sudo docker rm -f $(sudo docker stop $(sudo docker ps -a -q  --filter ancestor=isabella6151/airflow))
# kill `pgrep python`
