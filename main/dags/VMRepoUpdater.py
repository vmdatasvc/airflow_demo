import sys,os
import subprocess
import time

def startMongo():
    os.chdir(r'C:\VM\VPU_Tools\meepo')
    mongoResp = ''
    try:
        processMongo = subprocess.Popen(["sc", "start", "mongodb"],
                                        stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        processMongo.wait()
        mongoResp, err = processMongo.communicate()
    except:
        pass
    return mongoResp
def checkForUpdate():
    mongoLockFile = r'C:\MongoData\mongod.lock'
    mongoResp = startMongo()

    if 'instance of the service is already running' in mongoResp  or 'STATE:4RUNNING' in mongoResp.replace(' ',''):
        print 'Mongo up and running'
    else:

        os.remove(mongoLockFile)
        time.sleep(5)
        mongoResp = startMongo()
        if 'instance of the service is already running' in mongoResp or 'STATE:4RUNNING' in mongoResp.replace(' ', ''):
            print 'Mongo up and running after deleting the lock file'


    os.chdir(r'C:\VM\VPU_Tools\meepo')
    process = subprocess.Popen(["git", "-C", "C:\VM\VPU_Tools\meepo", "remote", "show", "origin"],cwd = 'C:\VM\VPU_Tools\meepo',
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()
    stdout, stderr = process.communicate()

    if 'local out of date' in stdout:
        # updating repoFirst
        process1 = subprocess.Popen(["sc","stop","VMSvc-Meepo"],stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        process1.wait()
        stdout1, stderr1 = process1.communicate()
        path = os.sep.join([os.path.dirname(os.path.abspath(__file__)), 'meepo_updater.bat'])
        # os.system(path)
        p = subprocess.Popen(path)
        p.wait()
        p.communicate()

if __name__ == '__main__':
    checkForUpdate()