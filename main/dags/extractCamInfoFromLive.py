try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import sys
import os.path
import datetime
import DBConnector as dbConn
import collections
import DBConnector
from bs4 import BeautifulSoup as bs


uploadRgnInfo = """
INSERT IGNORE INTO
    <table>
VALUES <valueStr>;
"""

dbSettings = collections.namedtuple('dbSettings',['host','user','password','schema','table'])
# AWS_DB_Prod_name = 'production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com'
# AWS_DB_Test_name = 'vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com'
# AWS_DB_USER = 'omniadmin'
# AWS_DB_PWD = 'videoMININGlabs2015'
vmhq_host = 'vmdb_master.videomining.com'
vmhq_user = 'vmsuser'
vmhq_pwd = 'irrelev@nt'
# AWS_DB_PWD = 'videoMININGLabs2016'

class extract:
    def __init__(self,optsDict,flag=''):
        self.op = optsDict
        self.dbObj = DBConnector.DBConnector(host = self.op['liveProjectSchema'],user = self.op['user'], password = self.op['password'], database='vms')
        self.flag = flag
        self.fetchVSFile()


    def fetchVSFile(self):
        self.__get_proj_id()
        self.__get_proj_version()
        self.__get_proj_soup()
        if self.flag.lower() == 'fac':
            self.__extractCamInfoFac()
        else:
            self.__extractCamInfo()

    def getCamInfo(self):
        return self.eventId

    def __extractCamInfo(self):
        self.eventId ={}
        camera_views = self.vision_settings_file.find_all('camera_view')
        trajId = ''
        for cam in camera_views:
            camera_viewsId = cam['id']

            if cam.find('type_label').text == 'Camera View' and 'cam' in cam.find('name').string and 'hires' not in cam.find('name').string.lower() and 'highres' not in cam.find('name').string.lower():
                camName = str(cam.find('name').string)
                find_all_event_channel = cam.find_all('event_channel')

                for template in find_all_event_channel:
                    if 'trajectory_generator2' in template.find('template'):
                        trajId = template.find('id').string

                    elif 'OmniTrajectory' in template.find('template'):
                        trajId = template.find('id').string

                eventId = 'event_'+ str(self.proj_id)+ '_' + str(camera_viewsId)+ '_' + str(trajId)
                self.eventId[camName]= eventId

    def __extractCamInfoFac(self):
        self.eventId = {}
        camera_views = self.vision_settings_file.find_all('camera_view')
        for cam in camera_views:
            camera_viewsId = cam['id']

            if cam.find('type_label').text == 'Camera View' and 'fac' in cam.find(
                    'name').string and 'hires' not in cam.find('name').string.lower() and 'highres' not in cam.find(
                    'name').string.lower():
                camName = str(cam.find('name').string)
                variableSet = cam.find_all('variable')
                for variable in variableSet:
                    if variable['name'] == 'OmniSensr.Demographics':
                        if variable['value'] == '1':
                            eventId = 'event_' + str(self.proj_id) + '_' + str(camera_viewsId)
                            self.eventId[camName] = eventId
                # find_all_event_channel = cam.find_all('event_channel')

                # for template in find_all_event_channel:
                #     if 'trajectory_generator2' in template.find('template'):
                #         trajId = template.find('id').string
                #
                #     elif 'OmniTrajectory' in template.find('template'):
                #         trajId = template.find('id').string

                # eventId = 'event_' + str(self.proj_id) + '_' + str(camera_viewsId) + '_' + str(trajId)
                # self.eventId[camName] = eventId



    def __get_proj_id(self):
        base_sql = '''
        SELECT
            project_id
        FROM
            vms.project_info
        WHERE
            name = '<proj_name>';
        '''
        sql = base_sql.replace('<proj_name>', self.op['proj_name'])
        retval = self.dbObj.execute(sql)
        if len(retval) == 0:
            raise Exception("There arent any projects in %s with name %s" %(self.op['db_server'], self.op['proj_name']))
        elif len(retval) > 1:
            raise Exception("There is more than one project in %s with name %s" %(self.op['db_server'], self.op['proj_name']))
        else:
            self.proj_id = int(retval[0][0])

    #Get the project version associated with the date given in the optsFile
    def __get_proj_version(self):
        base_sql = '''
        SELECT max(version)
        FROM vms.version_history
        where project_id = <proj_id>
        and DATE(modification_time) <= '<date>';
        '''
        sql = base_sql.replace('<proj_id>', str(self.proj_id)).replace('<date>', str(self.op['startDate']))
        retval = self.dbObj.execute(sql)[0][0]
        if retval:
            self.version = retval
        else:
            raise Exception('Could not find a version containing the given date.')

    #Get project blob file'''
    def __get_proj_soup(self):
        base_sql = '''
        SELECT
            data
        FROM
            vms.project
        WHERE
            id = <proj_id>
            and version = <version>;
        '''

        sql = base_sql.replace('<proj_id>', str(self.proj_id)).replace('<version>', str(self.version))

        retval = self.dbObj.execute(sql)
        if len(retval) == 0:
            raise Exception("No project found for ID %d" % self.proj_id)
        else:
            self.vision_settings_file = bs(str(retval[0][0]), 'xml')

        self.vision_settings_file.event_storage_type.string = 'embedded'

def sringToTupleList(str):
    outList = []
    strList = str.split(',')
    for idx in range(0,len(strList),2):
        outList.append((float(strList[idx]),float(strList[idx+1])))
    return outList

def extractProjectFileLocInfo(projFileName,dbInfo):
    dbObj = dbConn.DBConnector(host=dbInfo.host, user=dbInfo.user, password=dbInfo.password,database=dbInfo.schema)
    projLocSql = """
    SELECT host_name from
        <table>
    where name = '<proj_file_name>'
    """

    sql = projLocSql.replace('<table>',dbInfo.table).replace('<proj_file_name>',projFileName)
    retval = dbObj.execute(sql)
    try:
        proj_host = retval[0][0]
    except:
        print 'No project file found for the storeID specified. Please check and retry'
        sys.exit()

    return proj_host

def extractCamInfo(storeId,dateStr,flag = ''):
    dbInfo = dbSettings(vmhq_host, vmhq_user, vmhq_pwd, 'vms','all_project_info_fed')

    if 'live' not in storeId:
        projFileName = storeId+'_live'
    else:
        projFileName = storeId
    proj_host = extractProjectFileLocInfo(projFileName, dbInfo)
    # if proj_host == 'AWS_DB_TEST':
        # proj_host = AWS_DB_Test_name
    proj_host = proj_host+'.videomining.com'
    print 'connecting to host %s'%proj_host
    optsDict =  {'liveProjectSchema': proj_host,'user': dbInfo.user,'password':dbInfo.password,'proj_name':projFileName,'startDate':dateStr}

    ex = extract(optsDict,flag)
    CamInfo = ex.getCamInfo()
    return CamInfo, proj_host


if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
       sys.exit("\nusage: %s <store> <optional_date>\n"%os.path.basename(__file__))

    # validate opts file - opcodes and all the keywords
    storeId = sys.argv[1]
    if len(sys.argv) == 2:
        dateStr = '99991231'
    else:
        dateStr = sys.argv[2]
    try:
        dateVal = datetime.datetime.strptime(dateStr,'%Y%m%d')
    except:
        print 'Date Entered is incorrect. Please try with correct date value in format -YYYYMMDD'
        sys.exit()
    try:
        if '-' not in storeId:
            raise
        storeNum = long(storeId.split('-')[-1])
    except:
        print 'Store ID entered is incorrect. Please follow the following format -retailer-storeID e.g,swy-0413'
        sys.exit()
    CamInfo, proj_host = extractCamInfo(storeId,dateStr)
    # CamInfo, proj_host = extractCamInfo(storeId, dateStr, 'fac')


