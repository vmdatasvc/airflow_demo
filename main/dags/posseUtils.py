from yaml import load, Loader
import datetime
import os
import os.path

def _ParseOptsFile(filename):
    from yaml import load, Loader
    return load(open(filename,'r'),Loader=Loader)

    
def _getFirstLastDay(dateObj, maxDateObj):
    delta = datetime.timedelta(days=1)
    while dateObj.strftime("%A") != "Wednesday":
            dateObj -= delta
    startDayObj = dateObj
    stopDayObj = startDayObj + 6*delta
    if stopDayObj > maxDateObj:
        stopDayObj = maxDateObj
    return dateObj, stopDayObj
    
    
def _BreakIntoWeeks(dateTup):
    start = dateTup[0]
    stop = dateTup[1]
    delta = datetime.timedelta(days=1)
    startdateObj = datetime.datetime(int(start[:4]),int(start[4:6]),int(start[6:]))
    stopdateObj = datetime.datetime(int(stop[:4]),int(stop[4:6]),int(stop[6:]))
    curr = startdateObj
    dateRangeList = []
    while curr <= stopdateObj:
        firstDay,lastDay = _getFirstLastDay(curr,stopdateObj)
        rangeStart = curr
        i = 1
        while (curr + i * delta) <= lastDay:
            i += 1
        rangeStop = curr + ((i-1)*delta)
        curr += (i*delta)
        dateRangeList.append((rangeStart.strftime("%Y%m%d"), rangeStop.strftime("%Y%m%d")))
    return dateRangeList

    
def _ParseDateRangeDbUpload(dateRange):
    dateList = []
    dateRange = dateRange.strip()
    argv = dateRange.split(" ")
    while len(argv) > 0:
        if argv[0] == 'A':
            date = argv[1]
            dateList.append((date,date))
            argv = argv[2:]
        elif argv[0] == 'R':
            start = argv[1]
            stop = argv[2]
            dateRange = _BreakIntoWeeks((start,stop))
            dateList.extend(dateRange)
            argv = argv[3:]
        else:
            subStr = ''
            for arg in argv:
                subStr += ' %s'%arg
            raise Exception("\n\nMissing 'R' -or- 'A' in sub-string: '%s' of '%s'\n\n"%(subStr,dateRange))
    return dateList

def _getStartStopDate(dateList):
    startDt = datetime.datetime.strptime("99991231","%Y%m%d")
    stopDt = datetime.datetime.strptime("10000101","%Y%m%d")
    for date in dateList:
        dt = datetime.datetime.strptime(date,"%Y%m%d")
        if dt < startDt:
            startDt = dt
        if  dt > stopDt:
            stopDt = dt
    startDate = datetime.datetime.strftime(startDt,"%Y%m%d")
    stopDate = datetime.datetime.strftime(stopDt,"%Y%m%d")
    return startDate,stopDate
    
def _ParseDateRange(dateRange):
    dateList = []
    print dateRange
    dateRange = dateRange.strip()
    timeDelta = datetime.timedelta(1,0,0)
    argv = dateRange.split(" ")
    while len(argv) > 0:
        if argv[0] == 'A':
            date = argv[1]
            argv = argv[2:]
            date = datetime.datetime(int(date[0:4]),int(date[4:6]), int(date[6:]), 0, 0, 0, 0)
            dateList.append(date.strftime("%Y%m%d"))
        elif argv[0] == 'R':
            startDate = argv[1]
            stopDate = argv[2]
            argv = argv[3:]
            startDate = datetime.datetime(int(startDate[0:4]),int(startDate[4:6]), int(startDate[6:]), 0, 0, 0, 0)
            stopDate = datetime.datetime(int(stopDate[0:4]),int(stopDate[4:6]), int(stopDate[6:]), 0, 0, 0, 0)
            currDate = startDate
            while currDate <= stopDate:
                dateList.append(currDate.strftime("%Y%m%d"))
                currDate = currDate + timeDelta
        else:
            subStr = ''
            for arg in argv:
                subStr += ' %s'%arg
            raise Exception("\n\nMissing 'R' -or- 'A' in sub-string: '%s' of '%s'\n\n"%(subStr,dateRange))                
    return dateList


def _ParseCamRange(camRange):
    camList = []
    if camRange is None:
        return camList
    argv = camRange.split(" ")

    while len(argv) > 0:
        if argv[0] == 'R':
            startCam = int(argv[1])
            stopCam = int(argv[2])
            argv = argv[3:]
            camList += range(startCam, stopCam + 1)
        elif argv[0] == 'A':
            cam = argv[1]
            argv = argv[2:]
            camList.append(int(cam))

    return camList