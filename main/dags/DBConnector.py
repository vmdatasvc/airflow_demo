import warnings
with warnings.catch_warnings():
    warnings.filterwarnings('ignore', category=DeprecationWarning)
    import MySQLdb

import pdb
import sys

# class DBSettings:
    # host = 'production-1.cluster-cetyenwapymy.us-east-1.rds.amazonaws.com'
    # user = 'omniadmin'
    # password = 'videoMININGLabs2016'

class DBSettings:
    host = 'vmiotstore.cxbttlf9n1ld.us-east-1.rds.amazonaws.com'
    user = 'omniadmin'
    password = 'videoMININGlabs2015'

class DBConnector:
    import exceptions
    cursor = ''
    dbSettings = DBSettings()
    db = ''
    database = 'region_info'
    def __init__(self,host = DBSettings.host,user=DBSettings.user,password=DBSettings.password,database='region_info'):
        self.__connectToDB(host,user,password,database)

    def __connectToDB(self, host, user, password, database='region_info'):
        # check if cursor is active, if so terminate the old connection and initiate a new one
        try:
            db = MySQLdb.connect(host=host, db=database, user=user, passwd=password )
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            sys.exit (1)
        self.db = db
        self.database = database
        self.cursor = db.cursor()
        self.dictCursor = db.cursor(MySQLdb.cursors.DictCursor)
    
    def __del__(self,):
        print "Closing connections to database\n"
        self.cursor.close()
        self.dictCursor.close()
        self.db.close()


    def execute(self,SQLQueries,valList=''):
        if isinstance(SQLQueries, type('')) and valList == '':
            self.__exec(SQLQueries)
            retString = self.cursor.fetchall()
        elif valList != '':
            self.__execMany(SQLQueries,valList)
            retString = self.cursor.fetchall()
        else:
            print SQLQueries
            raise Exception("unknown SQLQuery")
        return retString
    def executeAsDict(self,SQLQueries,valList=''):
        if isinstance(SQLQueries, str) and valList == '':
            self.__execAsDict(SQLQueries)
            retString = self.dictCursor.fetchall()
        elif valList != '':
            self.__execManyAsDict(SQLQueries,valList)
            retString = self.dictCursor.fetchall()
        else:
            print SQLQueries
            raise Exception("unknown SQLQuery")
        return retString 
    def __exec(self,sqlString):
        try:
            retVal = self.cursor.execute(sqlString)
            # print retVal
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            sys.exit (1)
    def __execMany(self,sql,valList):
        assert valList != ''
        try:
            retVal = self.cursor.executemany(sql, valList)
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            sys.exit (1)    
    def __execAsDict(self,sqlString):
        try:
            retVal = self.dictCursor.execute(sqlString)
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            sys.exit (1)    
    def __execManyAsDict(self,sql, valList):
        assert valList != ''
        try:
            retVal = self.dictCursor.executemany(sql,valList)
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            sys.exit (1)
    
    def commit(self):
        self.db.commit()
        
    def execInsertSql(self,inSql,valList=''):
        self.execute(inSql,valList)
        self.commit()
        
    def execSelectSql(self,inSql,valList=''):
        return self.execute(inSql,valList='')
        
    def escape_string(self, input_str):
        return self.db.escape_string(input_str)
    