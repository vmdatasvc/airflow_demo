import FaceProcessor
from airflow.operators import PythonOperator
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
import posseUtils
from datetime import datetime,timedelta
import extractCamInfoFromLive
import createUserForAuth
bucket_name = 'vml-binaries-videomining'
file_type = 'branches/ec2/origin/master/build/bin'



def main_demographics(startDateHour, endDateHour, storeName, facCam, timeZone):
    FaceProcessor.FacePostProccessorDmg(startDateHour, endDateHour, storeName, facCam, timeZone)

def main_joinBrokenTracks(startDateHour, endDateHour, storeName, facCam, timeZone):
    FaceProcessor.FacePostProcessorJbt(startDateHour, endDateHour, storeName, facCam, timeZone)

def parseConfig(optsFile):
    optsDict = posseUtils._ParseOptsFile(optsFile)
    return optsDict

def initDag():
    default_args = {
        'owner': 'airflow',
        'depends_on_past': False,
        'start_date': datetime(2017, 8, 05),
        # 'email': ['nnayak@videomining.com'],
        # 'email_on_failure': False,
        # 'email_on_retry': False,
        # 'retries': 1,
        # 'retry_delay': timedelta(minutes=5),
        # 'queue': 'bash_queue',
        # 'pool': 'backfill',
        # 'priority_weight': 10,
        # 'end_date': datetime(2016, 1, 1),
    }

    dag_face = DAG('faceProcess', default_args=default_args,schedule_interval=timedelta(days=1))
    return dag_face


    
def loadAllTasks(optsFile,dag_face):
    #load the optsfile first
    optsDict = posseUtils._ParseOptsFile(optsFile)

    dateList = posseUtils._ParseDateRange(optsDict['dateRange'])
    print dateList
    storeName = optsDict['storeName']
    timeZone = optsDict['timeZone']
    demographicTask = {storeName:{}}
    joinBrokenTask = {storeName:{}}
    print "creating tasks"
    for date in dateList:
        demographicTask[storeName][date] = {}
        joinBrokenTask[storeName][date] = {}
        startDateHour = date + 'T0100'
        endDateHour = date + 'T0600'
        CamInfo, proj_host = extractCamInfoFromLive.extractCamInfo(storeName, date,'fac')
        store = storeName.replace('-','_')
        camList = CamInfo.keys()
        # runTime = datetime.strftime(datetime.utcnow(),'%Y%m%d%H%M%S')
        for cam in camList:
            # main_demographics(startDateHour, endDateHour, storeName, cam, timeZone)

            # inputFile = baseDir+'/cam%03d_%s.csv'%(cam,str(date))
            demographicTask[storeName][date][cam] = PythonOperator(
                task_id = 'demo_%s_%s_%s'%(storeName,date,cam),
                python_callable = main_demographics,
                op_kwargs = {'startDateHour':startDateHour, 'endDateHour': endDateHour, 'storeName':store, 'facCam':cam, 'timeZone':timeZone},
                dag = dag_face)
            joinBrokenTask[storeName][date][cam] = PythonOperator(
                task_id='jbt_%s_%s_%s' % (storeName, date, cam),
                python_callable=main_joinBrokenTracks,
                op_kwargs={'startDateHour': startDateHour, 'endDateHour': endDateHour, 'storeName': store,
                           'facCam': cam, 'timeZone': timeZone},
                dag=dag_face)

            joinBrokenTask[storeName][date][cam].set_upstream(demographicTask[storeName][date][cam])
    

optsFile = '/home/ubuntu/airflow/dags/6072_optsFile_omni.yml'
# #initialize the dag here
dag_face = initDag()
createUserForAuth.createUser()
# #load all the tasks here
loadAllTasks(optsFile,dag_face)





#
#
# optsFile = '/home/nnayak/Downloads/airflow_test/1751_optsFile.yml'
# #initialize the dag here
# dag_posse = initDag()
# #load all the tasks here
# loadSingeTask(optsFile,dag_posse)
#
#
#
#
#
#
